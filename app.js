// 导入 express
const express = require('express')

// 导入 body-parser 依赖包 
const body = require('body-parser');

// 创建服务器
const server = express()

// 设定在app.js上 所有的请求都会触发执行
// 以普通字符串形式携带参数
server.use(body({ extended: false }));
// 以json字符串形式携带参数
server.use(body.json());

// 导入路由
const table = require('./app/table.js')

// 静态请求
server.use('/static', express.static('./src'));

// 动态请求
server.use('/api', table)

// 添加服务器监听
server.listen('8088', function () { console.log('服务器正常启动 正在监听 8088 端口') })