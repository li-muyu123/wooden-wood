
// 导入 express
const express = require('express')

// 导入下一级
const {getTable} = require('./getTable.js')
const {setTable} = require('./setTable.js')

// 创建路由表
const router = express.Router();

router.get('/getTable', getTable)
router.post('/setTable', setTable)


// 导出路由表
module.exports = router