// 获取 tbody 标签
const oTbody = document.querySelector('tbody')
// 获取 div
const oDiv = document.querySelector('.bigbox')
// 获取添加弹窗
const oAdd = document.querySelector('.add')
// 获取编辑弹窗
const oChange = document.querySelector('.change')
// 定义变量存储点击的数据的索引下标
let index

// 定义变量存储json文件
let arr

setPage()
// 渲染函数
async function setPage() {
  const res = JSON.parse(await myPromiseAjax({
    type: 'get',
    url: '/api/getTable'
  }))
  arr = res
  console.log(res)
  let str = ``
  res.forEach((item, index) => {
    str += `
    <tr>
      <td>${item.id}</td>
      <td>${item.name}</td>
      <td>${item.iphone}</td>
      <td>${item.email}</td>
      <td>
        <p class="${item.sex === '男' ? 'nan' : 'nv'}">${item.sex}</p>
      </td>
      <td>
        <p class="${item.state === 'true' ? 'avail' : 'dis'}">${item.state === 'true' ? '可用' : '禁用'}</p>
      </td>
      <td>
        <button index="${index}" class="changeBtn">编辑</button>
        <button index="${index}" class="delBtn">删除</button>
      </td>
    </tr>
    `
  })
  oTbody.innerHTML = str
}


// 给div添加点击事件委托
oDiv.addEventListener('click', e => {
  // 点击添加
  if (e.target.className === 'addBtn') {
    oAdd.style.display = 'flex'
  }
  // 点击编辑
  else if (e.target.className === 'changeBtn') {
    oChange.style.display = 'flex'
    // 将对应的数据渲染到编辑页面
    index = Number(e.target.getAttribute('index'))
    oChange.querySelector('[name = name]').value = arr[index].name
    oChange.querySelector('[name = email]').value = arr[index].email
    oChange.querySelector('[name = iphone]').value = arr[index].iphone
    oChange.querySelectorAll('[name = sex').forEach(item => {
      if(item.value === arr[index].sex){
        item.checked = true
        console.log(item.value)
        console.log(arr[index].sex)
      }
    })
  }
  // 点击删除
  else if (e.target.className === 'delBtn') {

    if (window.confirm('您确定删除该信息吗?')) {
      // 调用发送请求
      arr.splice(Number(e.target.getAttribute('index')), 1)
      sendRequest('post', '/api/setTable', arr)
      setPage()

    }
  }
})


// 给增加div添加点击事件委托
oAdd.addEventListener('click', e => {
  // 点击×或者取消按钮
  if (e.target.className === 'false') {
    oAdd.style.display = 'none'
    // 点击确认按钮
  } else if (e.target.className === 'true') {
    // 获取需要添加的信息
    let name = oAdd.querySelector('[name = name]').value
    let pwd = oAdd.querySelector('[name = pwd]').value
    let email = oAdd.querySelector('[name = email]').value
    let sex
    oAdd.querySelectorAll('[name = sex').forEach(item => {
      if(item.checked){
        sex = item.value
      }
    })
    let iphone = oAdd.querySelector('[name = iphone]').value

    let obj = {
      id: arr.length === 0 ? 1 : arr[arr.length - 1].id + 1,
      name: name,
      pwd: pwd,
      iphone: iphone,
      email: email,
      sex: sex,
      state: "false"
    }
    // 调用发送请求
    arr.push(obj)
    sendRequest('post', '/api/setTable', arr)
    setPage()
    oAdd.style.display = 'none'
  }
})

// 给编辑div添加点击事件委托
oChange.addEventListener('click', e => {
  if (e.target.className === 'false') {
    oChange.style.display = 'none'
  }else if(e.target.className === 'true'){
     // 获取需要添加的信息
     let name = oChange.querySelector('[name = name]').value
     let pwd = oChange.querySelector('[name = pwd]').value
     let email = oChange.querySelector('[name = email]').value
     let sex
     oChange.querySelectorAll('[name = sex').forEach(item => {
       if(item.checked){
         sex = item.value
       }
     })
     let iphone = oChange.querySelector('[name = iphone]').value
 
     arr[index] = {
       id: arr[index].id,
       name: name,
       pwd: pwd,
       iphone: iphone,
       email: email,
       sex: sex,
       state: arr[index].state
     }
     // 调用发送请求
     sendRequest('post', '/api/setTable', arr)
     setPage()
     oChange.style.display = 'none'
  }
})


// 发送请求函数
async function sendRequest(type, url, data) {
  if (type === 'get') {
    return JSON.parse(await myPromiseAjax({
      type: type,
      url: url
    }))
  } else if (type === 'post') {
    return JSON.parse(await myPromiseAjax({
      type: type,
      url: url,
      data: JSON.stringify(data)
    }))
  }

}