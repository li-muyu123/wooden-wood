// 获取登录按钮
const oBtn = document.querySelector('button')
// 获取用户名
const name = document.querySelector('[name = name]')
// 获取密码
const pwd = document.querySelector('[name = pwd]')
// 获取提示框
const oP = document.querySelector('p')
oBtn.addEventListener('click' , ()=>{
  console.log(name.value)
  if(!/^\w{2,4}$/.test(name.value)){
    oP.innerHTML = '请输入数字/字母/下划线2-4位账号'
    return
  }else{
    oP.innerHTML = ''
  }

  if(!/^\d{6}$/.test(pwd.value)){
    oP.innerHTML = '请输入六位数密码'
    return
  }else{
    oP.innerHTML = ''
  }
  window.location.href = '/static/pages/index.html'
})  